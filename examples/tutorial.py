
# type exec(open("tutorial.py").read()) to run script in IDLE.

import verysimplemodule

x = 2.3
y = 5.4

print(f"add: {verysimplemodule.add(x,y)}")
print(f"subtract: {verysimplemodule.subtract(x,y)}")
print(f"multiply: {verysimplemodule.extras.multiply(x,y)}")
print(f"divide: {verysimplemodule.extras.divide(x,y)}")
